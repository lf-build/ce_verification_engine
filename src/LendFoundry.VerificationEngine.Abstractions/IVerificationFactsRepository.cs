﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationFactsRepository : IRepository<IVerificationFacts>
    {
        Task<IVerificationFacts> Get(string entityType, string entityId,string factName);
        Task<IVerificationFacts> AddOrUpdate(IVerificationFacts verificationFact);
        Task<List<IVerificationFacts>> Get(string entityType, string entityId, List<string> facts, CurrentStatus status);
        Task<List<IVerificationFacts>> Get(string entityType, string entityId);

        Task<List<IVerificationFacts>> Get(string entityType, string entityId, CurrentStatus currentStatus);


    }
}