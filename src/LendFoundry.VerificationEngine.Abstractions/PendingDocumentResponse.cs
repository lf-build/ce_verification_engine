﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.VerificationEngine
{
    public class PendingDocumentResponse : IPendingDocumentResponse
    {
        public string FactName { get; set; }
        public string MethodName { get; set; }
        public List<string> DocumentName { get; set; }
    }
}
