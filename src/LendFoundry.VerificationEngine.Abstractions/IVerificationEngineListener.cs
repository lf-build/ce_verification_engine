﻿namespace LendFoundry.VerificationEngine
{
    public interface IVerificationEngineListener
    {
        void Start();
    }
}