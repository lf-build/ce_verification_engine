﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.VerificationEngine.Configuration
{
    public interface IStatusChange
    {
        string ChangeStatusCode { get; set; }

        List<string> FactNames { get; set; }
    }
}
