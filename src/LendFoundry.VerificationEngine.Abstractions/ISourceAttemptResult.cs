﻿using System;
using LendFoundry.VerificationEngine.Configuration;
using LendFoundry.Foundation.Date;

namespace LendFoundry.VerificationEngine
{
    public interface IMethodAttemptResult
    {
        VerificationResult Result { get; set; }
        TimeBucket DateAttempt { get; set; }
        string VerifiedBy { get; set; }

        VerificationRule RuleExecuted { get; set; }

        string SourceName { get; set; }
    }
}