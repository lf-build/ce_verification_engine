﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.VerificationEngine
{
    public interface IDocument
    {
         string DocumentGeneratedId { get; set; }

         DocumentStatus DocumentStatus { get; set; }

         List<string> Reasons { get; set; }
      
    }
}
