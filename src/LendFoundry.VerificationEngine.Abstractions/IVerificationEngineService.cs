﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.VerificationEngine.Configuration;
using LendFoundry.Application.Document;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationEngineService
    {
        Task<IVerificationMethod> Verify(string entityType, string entityId, string factName, string methodName, string sourceName, object request);
        Task<IVerificationFacts> GetStatus(string entityType, string entityId, string factName);
        Task<List<IVerificationMethod>> GetDetails(string entityType, string entityId, string factName);
        Task<List<IFact>> GetFactList(string entityType);
        Task<List<IFactResponse>> GetFactVerification(string entityType, string entityId);
        Task InitiateManualVerification(string entityType, string entityId, string factName, string currentInitiateMethod);
        Task<IApplicationDocument> AddDocument(string entityType, string entityId, string factName, string methodName, byte[] file, string fileName);
        Task<List<IPendingDocumentResponse>> GetPendingDocument(string entityType, string entityId);
        Task<List<IRequiredDocumentResponse>> GetRequiredDocumet(string entityType, string factName);

        Task<List<IDocumentResponse>> GetDocuments(string entityType, string entityId);
        Task EventRePublish(string entityType, string entityId);

        Task<List<IDocumentWithApplication>> GetDocumentsByFact(string entityType, string entityId, string factName);

        Task VerifyDocument(string entityType, string entityId, string factName, string documentId, string statusCode, List<string> reasons);
    }
}