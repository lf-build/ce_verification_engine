﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Security.Encryption;

namespace LendFoundry.VerificationEngine.Persistance
{
    public class VerificationMethodRepositoryFactory : IVerificationMethodRepositoryFactory
    {
        #region Constructor
        public VerificationMethodRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }
        #endregion

        #region Private Properties
        private IServiceProvider Provider { get; }
        #endregion

        #region Public Methods
        public IVerificationMethodRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            var encryptionService = Provider.GetService<IEncryptionService>();
            return new VerificationMethodRepository(tenantService, mongoConfiguration, encryptionService);
        }
        #endregion
    }
}