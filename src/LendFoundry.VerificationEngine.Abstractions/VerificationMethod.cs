﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.VerificationEngine
{
    public class VerificationMethod :Aggregate, IVerificationMethod
    {
        public VerificationMethod()
        {
            VerificationSources = new List<IVerificationSources>();
            Attempts = new List<IMethodAttemptResult>();
            Documents= new List<Document>();
            //DocumentsRequested=new List<string>();
        }

        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string FactName { get; set; }
        public string MethodName { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVerificationSources, VerificationSources>))]
        public List<IVerificationSources> VerificationSources { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IMethodAttemptResult,MethodAttemptResult>))]
        public List<IMethodAttemptResult> Attempts { get; set; }

        public List<Document> Documents { get; set; }

      
    }
}
 