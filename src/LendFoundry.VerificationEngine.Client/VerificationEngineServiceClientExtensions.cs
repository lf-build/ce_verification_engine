﻿using LendFoundry.Security.Tokens;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.VerificationEngine.Client
{
    public static class VerificationEngineServiceClientExtensions
    {
        public static IServiceCollection AddVerificationEngineService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IVerificationEngineServiceClientFactory>(p => new VerificationEngineServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IVerificationEngineServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
