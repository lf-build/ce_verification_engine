﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.VerificationEngine
{
    public class DocumentResponse : IDocumentResponse
    {
        public string FactName { get; set; }
        public string MethodName { get; set; }
        public List<string> DocumentName { get; set; }

        public bool IsRequested { get; set; }
    }
}
