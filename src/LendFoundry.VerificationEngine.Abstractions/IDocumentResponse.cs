﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.VerificationEngine
{
    public interface IDocumentResponse
    {
        string FactName { get; set; }
        string MethodName { get; set; }
        List<string> DocumentName { get; set; }

        bool IsRequested { get; set; }
    }
}
