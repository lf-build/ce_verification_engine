﻿using System.Collections.Generic;

namespace LendFoundry.VerificationEngine.Configuration
{
    public class VerificationConfiguration
    {
        public Dictionary<string,List<Fact>> Facts { get; set; }
        public List<EventMapping> Events { get; set; }
    }
}
