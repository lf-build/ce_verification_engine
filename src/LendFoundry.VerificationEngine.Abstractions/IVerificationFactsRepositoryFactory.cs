﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationFactsRepositoryFactory
    {
        IVerificationFactsRepository Create(ITokenReader reader);
    }
}
