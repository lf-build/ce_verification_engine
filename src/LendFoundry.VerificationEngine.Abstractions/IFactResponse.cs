﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using LendFoundry.Foundation.Date;

namespace LendFoundry.VerificationEngine
{
    public interface IFactResponse 
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string FactName { get; set; }
        CurrentStatus CurrentStatus { get; set; }
        VerificationStatus VerificationStatus { get; set; }
        string CurrentInitiatedMethod { get; set; }
        TimeBucket LastVerifiedDate { get; set; }
        string LastVerifiedBy { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IVerificationSources, VerificationSources>))]
        List<IVerificationSources> VerificationSources { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IMethodAttemptResult, MethodAttemptResult>))]
        List<IMethodAttemptResult> Attempts { get; set; }
    }
}