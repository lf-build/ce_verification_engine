﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace LendFoundry.VerificationEngine
{
    public class VerificationFacts : Aggregate, IVerificationFacts
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string FactName { get; set; }
        public CurrentStatus CurrentStatus { get; set; }
        public VerificationStatus VerificationStatus { get; set; }
        public TimeBucket LastVerifiedDate { get; set; }
        public string LastVerifiedBy { get; set; }

        public string CurrentInitiatedMethod { get; set; }

        [BsonIgnore]
        public string MethodName { get; set; }

        [BsonIgnore]
        public string RequestType { get; set; }
    }
}
