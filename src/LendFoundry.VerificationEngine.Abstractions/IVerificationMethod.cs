﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationMethod : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string FactName { get; set; }
        string MethodName { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVerificationSources, VerificationSources>))]
        List<IVerificationSources> VerificationSources { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IMethodAttemptResult, MethodAttemptResult>))]
        List<IMethodAttemptResult> Attempts { get; set; }

        List<Document> Documents { get; set; }
       
    }
}
