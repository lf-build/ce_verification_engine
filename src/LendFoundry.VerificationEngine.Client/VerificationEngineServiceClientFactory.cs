﻿using System;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;

namespace LendFoundry.VerificationEngine.Client
{
    public class VerificationEngineServiceClientFactory : IVerificationEngineServiceClientFactory
    {
        #region Constructors
        public VerificationEngineServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        #endregion

        #region Private Properties
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        #endregion

        #region Public Methods
        public IVerificationEngineService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new VerificationEngineService(client);
        }
        #endregion
    }
}
