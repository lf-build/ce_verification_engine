using System;
using System.Collections.Generic;
using LendFoundry.VerificationEngine.Configuration;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using LendFoundry.Foundation.Date;

namespace LendFoundry.VerificationEngine
{
    public class VerificationSources : IVerificationSources
    {
        public string SourceName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISource, Source>))]
        public ISource Source { get; set; }
        public object DataExtracted { get; set; }
        public TimeBucket DataExtractedDate { get; set; }
    }
}