﻿using System;


namespace LendFoundry.VerificationEngine
{
    public class VerificationFailedException : Exception
    {
        public VerificationFailedException(string message) : base(message)
        {

        }
    }
}
