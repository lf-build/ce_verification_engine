﻿using System;
using System.Collections.Generic;

namespace LendFoundry.VerificationEngine.Configuration
{
    public class Source : ISource
    {
        public SourceType Type { get; set; }
        public string Name { get; set; }      
        public Dictionary<string, string> Settings { get; set; }
       public List<string> RequiredDataAttributes { get; set; }

        public string SaveToDataAttribute { get; set; }
    }
}
