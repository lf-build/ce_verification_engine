namespace LendFoundry.VerificationEngine
{
    public enum DocumentStatus
    {
        Verifiable,
        Unverifiable       
    }
}