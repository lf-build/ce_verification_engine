﻿using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.VerificationEngine.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Configuration;

namespace LendFoundry.VerificationEngine
{
    public class VerificationEngineListener : IVerificationEngineListener
    {
        public VerificationEngineListener
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,        
            IVerificationEngineServiceFactory verificationEngineServiceFactory,
            ILookupClientFactory lookupFactory)
        {
            EventHubFactory = eventHubFactory;
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;           
            VerificationEngineServiceFactory = verificationEngineServiceFactory;
            LookupClientFactory = lookupFactory;           
        }

        #region Private Properties

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }   
        private IVerificationEngineServiceFactory VerificationEngineServiceFactory { get; }
        private ILookupClientFactory LookupClientFactory { get; }

        #endregion

        #region Public Methods

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Listener...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);           
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    logger.Info($"Processing tenant #{tenant.Id}");
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName, null, "system", null);
                    var reader = new StaticTokenReader(token.Value);                   
                    var eventHub = EventHubFactory.Create(reader);

                    var configuration = ConfigurationFactory.Create<VerificationConfiguration>(Settings.ServiceName, reader).Get();
                    var verificaitonService = VerificationEngineServiceFactory.Create(reader, TokenHandler, logger, configuration);

                    var lookupService = LookupClientFactory.Create(reader);

                    if (configuration == null)
                    {
                        logger.Error($"The configuration for service #{Settings.ServiceName} could not be found, please verify");
                    }
                    else
                    {
                        configuration
                       .Events
                       .ToList().ForEach(events =>
                        {
                            eventHub.On(events.Name, ProcessEvent(events, configuration, eventHub, logger, verificaitonService));
                            logger.Info($"It was made subscription to EventHub with the Event: #{events.Name}");
                        });

                        logger.Info("-------------------------------------------");
                    }
                    eventHub.StartAsync();
                });
              
                logger.Info("Fact verification listener started");
            }          
            catch (Exception ex)
            {
                logger.Error("Error while listening eventhub to process Fact verification", ex);
                logger.Info("\n Fact verification  is working yet and waiting new event\n");
                Start();
            }
        }

        #endregion

        #region Private Methods
        private Action<EventInfo> ProcessEvent
        (
            EventMapping eventConfiguration,
            VerificationConfiguration configuration,
            IEventHubClient eventHub,
            ILogger logger,
            IVerificationEngineServiceInternal verificationService
        )
        {
            return async @event =>
            {
               
                try
                {
                    var syndicationName = eventConfiguration.SyndicationName.FormatWith(@event);
                    var entityId = eventConfiguration.EntityId.FormatWith(@event);
                    var referenceNumber = eventConfiguration.ReferenceNumber.FormatWith(@event);
                    var entityType = eventConfiguration.EntityType.FormatWith(@event);

                    string responseData = eventConfiguration.Response.FormatWith(@event);
                    object data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseData);

                    logger.Info($"Syndication Name : #{syndicationName}");
                    logger.Info($"Entity Id: #{entityId}");
                    logger.Info($"Refernce Number: #{referenceNumber}");
                    logger.Info($"Response Data : #{data}");

                    entityType = entityType.ToLower();

                    var facts = EnsureFacts(configuration, entityType);

                   await VerifyFact(facts, verificationService, logger, entityId, syndicationName, entityType,new { data });
                   
                  
                    logger.Info($"Fact verified for {syndicationName} for {entityId} ");
                }
                catch (Exception ex)
                {
                    logger.Error($"Unhadled exception while listening event {@event.Name}", ex);
                }
            };
        }

        private static async Task VerifyFact(IEnumerable<Fact> facts, IVerificationEngineServiceInternal verificationService, ILogger logger, string entityId, string syndicationName, string entityType, object data)
        {
            IVerificationMethod verifiedFacts = null;

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is required");

            if (string.IsNullOrWhiteSpace(syndicationName))
                throw new ArgumentException("Syndication Name is required");

            foreach (var fact in facts)
            {
                if (fact.Methods == null) continue;

                foreach (var method in fact.Methods)
                {
                    if (method.Sources == null)
                        continue;

                   
                    foreach (var source in method.Sources)
                    {
                        try
                        {
                            if (source.Type != SourceType.Syndication || !source.Name.Equals(syndicationName))
                                continue;

                            verifiedFacts = await verificationService.Verify(entityType, entityId, fact.Name, method.Name, source.Name,false,new { data });
                        }
                        catch (Exception ex)
                        {
                            logger.Error($"Unhadled exception while verifying fact {fact.Name}", ex);
                        }
                    }
                }
            }           
        }

        private IEnumerable<Fact> EnsureFacts(VerificationConfiguration configuration, string entityType)
        {
            if (configuration == null)
                throw new NotFoundException("Configuration is not set.");

            if (!configuration.Facts.ContainsKey(entityType))
                throw new NotFoundException($"The fact not found for {entityType}");

            var facts = configuration.Facts[entityType];

            if (facts == null || !facts.Any())
                throw new NotFoundException($"The fact not found for {entityType}");
            return facts;
        }
        #endregion
    }
}


