﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Foundation.Date;

namespace LendFoundry.VerificationEngine.Persistance
{
    public class VerificationFactsRepository : MongoRepository<IVerificationFacts, VerificationFacts>,
        IVerificationFactsRepository
    {
        static VerificationFactsRepository()
        {
            BsonClassMap.RegisterClassMap<VerificationFacts>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.CurrentStatus).SetSerializer(new EnumSerializer<CurrentStatus>(BsonType.String));
                map.MapProperty(p => p.VerificationStatus).SetSerializer(new EnumSerializer<VerificationStatus>(BsonType.String));
               // map.MapProperty(p => p.LastVerifiedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(VerificationFacts);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
        }

        public VerificationFactsRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "facts")
        {
            CreateIndexIfNotExists("facts_id",
                Builders<IVerificationFacts>.IndexKeys.Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId)
                    .Ascending(i => i.FactName));
        }

        public async Task<IVerificationFacts> Get(string entityType, string entityId, string factName)
        {
            return await Task.Run(() =>
            {
                return Query.Where(fact => fact.EntityType == entityType && fact.EntityId == entityId && fact.FactName == factName).FirstOrDefault();
            });
        }

        public async Task<IVerificationFacts> AddOrUpdate(IVerificationFacts verificationFact)
        {
            var fact = await Get(verificationFact.EntityType, verificationFact.EntityId, verificationFact.FactName);

            if (fact == null)
            {
                Add(verificationFact);
                return verificationFact;
            }

            fact.CurrentStatus = verificationFact.CurrentStatus;
            fact.LastVerifiedBy = verificationFact.LastVerifiedBy;
            fact.LastVerifiedDate = verificationFact.LastVerifiedDate;
            fact.VerificationStatus = verificationFact.VerificationStatus;
            fact.CurrentInitiatedMethod = verificationFact.CurrentInitiatedMethod;
            Update(fact);

            return fact;
        }

        public async Task<List<IVerificationFacts>> Get(string entityType, string entityId, List<string> facts, CurrentStatus status)
        {
            return await Query.Where(fact => fact.EntityType == entityType && fact.EntityId == entityId && facts.Contains(fact.FactName) && fact.CurrentStatus == status).ToListAsync();
        }

        public async Task<List<IVerificationFacts>> Get(string entityType, string entityId)
        {
            return await Query.Where(fact => fact.EntityType == entityType && fact.EntityId == entityId).ToListAsync();
        }

        public async Task<List<IVerificationFacts>> Get(string entityType, string entityId, CurrentStatus status)
        {
            return await
                Query.Where(
                    fact => fact.EntityType == entityType &&
                            fact.EntityId == entityId && fact.CurrentStatus == status
                ).ToListAsync();
        }


    }
}
