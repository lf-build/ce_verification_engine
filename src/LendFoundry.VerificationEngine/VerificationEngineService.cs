﻿#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.VerificationEngine.Configuration;
using LendFoundry.ProductRule;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Security.Tokens;
using Microsoft.CSharp.RuntimeBinder;
using SourceType = LendFoundry.VerificationEngine.Configuration.SourceType;
using LendFoundry.DataAttributes;
using LendFoundry.Security.Identity.Client;
using LendFoundry.StatusManagement;
using LendFoundry.Application.Document;

namespace LendFoundry.VerificationEngine
{
    public class VerificationEngineService : IVerificationEngineService, IVerificationEngineServiceInternal
    {
        #region Constructor

        public VerificationEngineService
        (
            IVerificationFactsRepository verificationFactRepository,
            IEventHubClient eventHubClient,
            ITenantTime tenantTime,
            IApplicationDocumentService applicationDocumentService,
            VerificationConfiguration configuration,
            IProductRuleService productRuleService,
            ILookupService lookup,
            IIdentityService identityService,
            ITokenReader tokenReader,
            ITokenHandler tokenParser, IVerificationMethodRepository verificationMethodRepository, IDataAttributesEngine dataAttributeEngine)
        {
            TenantTime = tenantTime;
            VerificationFactRepository = verificationFactRepository;
            EventHubClient = eventHubClient;
            ApplicationDocumentService = applicationDocumentService;
            Configuration = configuration;
            ProductRuleService = productRuleService;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            VerificationMethodRepository = verificationMethodRepository;
            DataAttributeEngine = dataAttributeEngine;
            IdentityService = identityService;
           
        }

        #endregion

        #region Private Properties
      
        private IIdentityService IdentityService { get; }
        private ITenantTime TenantTime { get; }
        private IVerificationFactsRepository VerificationFactRepository { get; }
        private IVerificationMethodRepository VerificationMethodRepository { get; }
        private VerificationConfiguration Configuration { get; }
        private IEventHubClient EventHubClient { get; }
        private IProductRuleService ProductRuleService { get; }

        private IApplicationDocumentService ApplicationDocumentService { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenParser { get; }
        private IDataAttributesEngine DataAttributeEngine { get; }

        #endregion

        #region Public Method
        public async Task<IVerificationMethod> Verify(string entityType, string entityId, string factName, string methodName, string sourceName, object request)
        {

            return await Verify(entityType, entityId, factName, methodName, sourceName, true, request);
        }

        public async Task<IVerificationMethod> Verify(string entityType, string entityId, string factName, string methodName, string sourceName, bool isManual, object input)
        {
            entityType = entityType.ToLower();
            var fact = EnsureFact(entityType, factName);
            var method = EnsureFactMethod(fact, methodName);
            var currentUser = EnsureCurrentUser();
            var source = method.Sources.FirstOrDefault(s => s.Name.ToLower() == sourceName.ToLower());

            if (source == null)
                throw new NotFoundException($"Source {sourceName} not found");

            if (method.RolesForAccess != null)
            {
                var currentUserRoles = await IdentityService.GetUserRoles(currentUser);
                if (currentUserRoles != null)
                {
                    var rolesExists = currentUserRoles.ToList().Intersect(method.RolesForAccess);
                    if (rolesExists == null)
                        throw new InvalidOperationException($"You don't have permission to verify {fact.Name} fact.");
                }
            }


            // If Document Upload is pending then user should not be able to upload document.
            if (source.Type == SourceType.Form || source.Type == SourceType.Review)
            {
                var PendingDocuments = await GetPendingDocument(entityType, entityId);
                var SourceDocumentPending = PendingDocuments.Where(i => i.MethodName == methodName).FirstOrDefault();
                if (SourceDocumentPending != null)
                    throw new InvalidOperationException("Document is Pending");
            }

            if (source.SaveToDataAttribute != null)
            {
                await DataAttributeEngine.SetAttribute(entityType, entityId, source.SaveToDataAttribute, input);
            }

            object attributes = null;
            if (source.RequiredDataAttributes != null && source.RequiredDataAttributes.Count > 0)
                attributes = await DataAttributeEngine.GetAttribute(entityType, entityId, source.RequiredDataAttributes);


            var verificationMethod =
                await VerificationMethodRepository.AddSource(entityType, entityId, fact.Name, method.Name,
                    new VerificationSources
                    {
                        DataExtractedDate =new TimeBucket(TenantTime.Now),
                        SourceName = sourceName,
                        DataExtracted = new { input = input, attributes = attributes },
                        Source = source
                    });
            VerificationResult verificationResult = VerificationResult.UnableToSpecify;
            if (CanExecuteRule(method, verificationMethod))
            {

                if (source.Type == SourceType.Automatic)
                {
                    verificationResult = GetVerificationResult(input);
                }
                else
                {
                  //  var extractedDataAllSource = ExtractedDataSources(verificationMethod);
                    verificationResult = await ExecuteRule(null, method, entityId, entityType);
                }
                if (verificationResult == VerificationResult.None)
                {
                    return verificationMethod;
                }
                var attemptResult = new MethodAttemptResult
                {
                    DateAttempt = new TimeBucket(TenantTime.Now),
                    VerifiedBy = currentUser,
                    Result = verificationResult,
                    SourceName = sourceName
                };
                verificationMethod = await VerificationMethodRepository.AddAttempt(verificationMethod.Id, attemptResult);
            }

            if (verificationResult == VerificationResult.UnableToSpecify)
            {
                await InitiateReviewMthodOnFailure(entityType, entityId, fact, method);
                throw new InvalidOperationException("Unable To Verify Fact");
            }

            var verificationFact = new VerificationFacts
            {
                EntityId = entityId,
                EntityType = entityType,
                FactName = fact.Name,
                LastVerifiedBy = currentUser,
                VerificationStatus = VerificationStatus.None,
                LastVerifiedDate = new TimeBucket(TenantTime.Now),
                CurrentStatus = CurrentStatus.InProgress,
                CurrentInitiatedMethod = method.Name

            };
            if (verificationMethod.Attempts == null || !verificationMethod.Attempts.Any())
            {
                verificationFact.CurrentStatus = CurrentStatus.InProgress;
                verificationFact.VerificationStatus = VerificationStatus.None;
                await VerificationFactRepository.AddOrUpdate(verificationFact);
                return verificationMethod;
            }
            if (verificationMethod.Attempts.Any(r => r.Result == VerificationResult.Passed))
            {

                verificationFact.CurrentStatus = CurrentStatus.Completed;
                verificationFact.VerificationStatus = VerificationStatus.Passed;
                verificationFact.MethodName = verificationMethod.Attempts.Where(i => i.Result == VerificationResult.Passed).OrderByDescending(i => i.DateAttempt.Time).FirstOrDefault().SourceName;
                var sourceNameDetail = verificationMethod.VerificationSources.Where(i => i.SourceName == verificationFact.MethodName).OrderByDescending(i => i.DataExtractedDate.Time).FirstOrDefault().Source.Type.ToString();
                verificationFact.RequestType = (sourceNameDetail == "Form" ? "Manual" : sourceNameDetail);
              
            }
            else
            {
                verificationFact.CurrentStatus = CurrentStatus.InProgress;
                verificationFact.VerificationStatus = VerificationStatus.Failed;
                verificationFact.MethodName = sourceName;
                verificationFact.RequestType = (source.Type.ToString() == "Form" ? "Manual" : source.Type.ToString());
            }
            await VerificationFactRepository.AddOrUpdate(verificationFact);

            // if fact is falied and review method is configured for the same , Initiate it .
            if ((verificationFact.CurrentStatus == CurrentStatus.InProgress) && (verificationFact.VerificationStatus == VerificationStatus.Failed))
                await InitiateReviewMthodOnFailure(entityType, entityId, fact, method);


            await EventHubClient.Publish(new FactVerificationCompleted { Fact = verificationFact });
            return verificationMethod;
        }

    


        private async Task InitiateReviewMthodOnFailure(string entityType, string entityId, Fact fact, IMethod method)
        {
            if (method.InitiateReviewonFailure)
            {
                var reviewMethod = fact.Methods.Where(i => i.IsReviewMethod == true).FirstOrDefault();
                if (reviewMethod != null)
                {
                    await InitiateManualVerification(entityType, entityId, fact.Name, reviewMethod.Name);
                }
            }
        }

        private static VerificationResult GetVerificationResult(object input)
        {
            VerificationResult verificationResult;
            if (input != null)
            {
                var result = GetResultValue(input);
                if (result == null)
                    throw new InvalidArgumentException("Request data is not in correct format");
                verificationResult = result.ToLower() == "true"
                    ? VerificationResult.Passed
                    : VerificationResult.Failed;
            }
            else
            {
                verificationResult = VerificationResult.UnableToSpecify;
            }

            return verificationResult;
        }

        public async Task<IVerificationFacts> GetStatus(string entityType, string entityId, string factName)
        {
            //if (string.IsNullOrWhiteSpace(entityType))
            //    throw new ArgumentException("Entity type is required");

            entityType = entityType.ToLower();

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            var fact = EnsureFact(entityType, factName);

            var verificationFact = await VerificationFactRepository.Get(entityType, entityId, fact.Name);
            if (string.IsNullOrEmpty(verificationFact?.Id))
                throw new NotFoundException($"{factName} is not initiated for {entityId}");
            return verificationFact;
        }

        public async Task<List<IVerificationMethod>> GetDetails(string entityType, string entityId, string factName)
        {
            entityType = entityType.ToLower();

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            var fact = EnsureFact(entityType, factName);

            var verificationDetails = await VerificationMethodRepository.Get(entityType, entityId, fact.Name);
            if (!verificationDetails.Any())
            {
                throw new NotFoundException($"{factName} is not initiated for {entityId}");
            }
            return verificationDetails;
        }

        public async Task<List<IDocumentWithApplication>> GetDocumentsByFact(string entityType, string entityId, string factName)
        {
            entityType = entityType.ToLower();
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            var fact = EnsureFact(entityType, factName);

            var verificationDetails = await VerificationMethodRepository.Get(entityType, entityId, fact.Name);
            if (!verificationDetails.Any())
            {
                throw new NotFoundException($"There are no records for {factName} and {entityId}");
            }

            var documentsList = await ApplicationDocumentService.GetByApplicationNumber(entityId);
            if (!documentsList.Any())
            {
                throw new NotFoundException($"There are no records for {factName} and {entityId} in application Documents");
            }
            var documentIds = new List<string>();
            foreach (var item in verificationDetails)
            {
                if (item.Documents != null && item.Documents.Count > 0)
                {
                    foreach (var documentItem in item.Documents)
                    {
                        documentIds.Add(documentItem.DocumentGeneratedId);
                    }
                }
            }
            return documentsList.Where(a => documentIds.Contains(a.Id)).ToList();
        }

        public async Task VerifyDocument(string entityType, string entityId, string factName, string documentId, string statusCode, List<string> reasons)
        {
            entityType = entityType.ToLower();
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            var fact = EnsureFact(entityType, factName);

            var verificationDetails = await VerificationMethodRepository.Get(entityType, entityId, fact.Name);
            if (!verificationDetails.Any())
            {
                throw new NotFoundException($"There are no records for {factName} and {entityId}");
            }
            var content = (DocumentStatus)Enum.Parse(typeof(DocumentStatus), statusCode);
            foreach (var item in verificationDetails)
            {
                if (item.Documents != null && item.Documents.Count > 0)
                {
                    var isDocumentExists = item.Documents.Where(i => i.DocumentGeneratedId == documentId).FirstOrDefault();
                    if (isDocumentExists != null)
                    {
                        isDocumentExists.DocumentStatus = content;
                        isDocumentExists.Reasons = reasons;
                        VerificationMethodRepository.Update(item);
                        if (statusCode == DocumentStatus.Unverifiable.ToString())
                        {
                            await UpdateFactDetails(entityType, entityId, factName);                           
                        }
                        await ApplicationDocumentService.VerifyDocument(entityId, documentId, statusCode); // once application document latest nuget available
                        return;
                    }
                }
            }
        }

        private async Task UpdateFactDetails(string entityType, string entityId, string factName)
        {
            var factDetails = await VerificationFactRepository.Get(entityType, entityId, factName);
            if (factDetails != null)
            {
                factDetails.CurrentStatus = CurrentStatus.Initiated;
                factDetails.VerificationStatus = VerificationStatus.None;
                VerificationFactRepository.Update(factDetails);
            }
        }

        public async Task<List<IFact>> GetFactList(string entityType)
        {
            entityType = entityType.ToLower();
            return await Task.Run(() =>
            {
                var facts = EnsureFacts(entityType).ToList();
                return new List<IFact>(facts);
            });
        }

        public async Task InitiateManualVerification(string entityType, string entityId, string factName, string currentInitiateMethod)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("entityId is required.");

            entityType = entityType.ToLower();

            var fact = EnsureFact(entityType, factName);
            var method = EnsureFactMethod(fact, currentInitiateMethod);
            var currentUser = EnsureCurrentUser();
            if (fact.IsOptional)
            {
                if (!string.IsNullOrEmpty(fact.OptionalFactGroupName))
                {
                    var result = await ProductRuleService.RunRule(entityType, entityId, "product1", fact.OptionalFactGroupName);

                    if (result == null)
                        throw new VerificationFailedException("Unable to execute rule : " + fact.OptionalFactGroupName);
                    ProductRuleResult ruleExecutionResult = result;


                    if (ruleExecutionResult.Result == Result.Failed)
                        throw new InvalidOperationException("Optional fact can not be initiated");

                }
            }
            await ChangeFactStatus(entityType, entityId, fact.Name, currentUser, CurrentStatus.Initiated, method.Name);

            await EventHubClient.Publish(new ManualFactVerificationInitiated(entityType, entityId, fact.Name));
        }

        public async Task<IApplicationDocument> AddDocument(string entityType, string entityId, string factName, string methodName, byte[] file, string fileName)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("entityId is required");

            entityType = entityType.ToLower();

            var fact = EnsureFact(entityType, factName);
            var method = EnsureFactMethod(fact, methodName);
            var currentUser = EnsureCurrentUser();
            var categoryName = method.Sources.FirstOrDefault().Settings["Category"];
            var documentName = method.Sources.FirstOrDefault().Settings["DocumentRequired"];
            var generatedDocument = await ApplicationDocumentService.Add(entityId, categoryName, file, fileName, null);

            if (string.IsNullOrEmpty(generatedDocument?.DocumentId))
                throw new DocumentException("Document is not uploaded successfully");

          
            await ChangeFactStatus(entityType, entityId, fact.Name, currentUser, CurrentStatus.InProgress, methodName);


            Document document = new Document();
            document.DocumentGeneratedId = generatedDocument?.DocumentId;
            document.DocumentStatus = DocumentStatus.Verifiable;
            await
                VerificationMethodRepository.AddDocuments(entityType, entityId, fact.Name, method.Name,
                    document);

            var PendingDocuments = await GetPendingDocument(entityType, entityId);

            await EventHubClient.Publish(new VerificationDocumentReceived(entityType, entityId, fact.Name, PendingDocuments.Count, currentUser, documentName));
            return generatedDocument;
        }

        public async Task<List<IPendingDocumentResponse>> GetPendingDocument(string entityType, string entityId)
        {
            entityType = entityType.ToLower();

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            var items = new List<IPendingDocumentResponse>();
            var pendingFacts = await VerificationFactRepository.Get(entityType, entityId, CurrentStatus.Initiated);
            foreach (var item in pendingFacts)
            {
                var configFact = EnsureFact(entityType, item.FactName);

                if (configFact == null)
                    throw new NotFoundException($"No verification facts are found for {entityType}");

                if (configFact.Methods == null || !configFact.Methods.Any())
                    return null;

                var currentMethodInitiated = configFact.Methods.Where(i => i.Name == item.CurrentInitiatedMethod).FirstOrDefault();

                // foreach (var method in configFact.Methods)
                //{
                if (currentMethodInitiated != null)
                {
                    foreach (var methodSource in currentMethodInitiated.Sources)
                    {
                        var documents = new List<string>();
                        if (methodSource.Type == SourceType.Form || methodSource.Type == SourceType.Review)
                        {
                            string document;
                            if (methodSource.Settings.TryGetValue("DocumentRequired", out document))
                            {
                                documents.Add(document);
                            }
                            if (document != null)
                                items.Add(new PendingDocumentResponse { FactName = configFact.Name, MethodName = currentMethodInitiated.Name, DocumentName = documents });
                        }
                    }
                    //}
                }
            }

            return items;

        }

        public async Task<List<IFactResponse>> GetFactVerification(string entityType, string entityId)
        {
            entityType = entityType.ToLower();

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            var facts = await VerificationFactRepository.Get(entityType, entityId);
            var methods = await VerificationMethodRepository.Get(entityType, entityId);
            List<IFactResponse> factResponse = null;
            if (facts != null)
            {
                factResponse = new List<IFactResponse>();
                foreach (var fact in facts)
                {
                    var response = new FactResponse
                    {
                        EntityType = entityType,
                        EntityId = entityId,
                        CurrentStatus = fact.CurrentStatus,
                        FactName = fact.FactName,
                        LastVerifiedBy = fact.LastVerifiedBy,
                        LastVerifiedDate = fact.LastVerifiedDate,
                        VerificationStatus = fact.VerificationStatus,
                        CurrentInitiatedMethod = fact.CurrentInitiatedMethod
                    };

                    if (methods != null && methods.Any())
                    {
                        var factMethods = methods.Where(m => m.FactName.ToLower() == fact.FactName.ToLower()).ToList();
                        if (factMethods.Any())
                        {
                            foreach (var method in factMethods)
                            {
                                response.Attempts.AddRange(method.Attempts);
                                response.VerificationSources.AddRange(method.VerificationSources);
                            }
                        }
                    }
                    factResponse.Add(response);
                }
            }

            return factResponse;
        }

        public async Task<List<IRequiredDocumentResponse>> GetRequiredDocumet(string entityType, string factName)
        {
            var items = new List<IRequiredDocumentResponse>();
            await Task.Run(() =>
            {

                entityType = entityType.ToLower();
                var configFact = EnsureFact(entityType, factName);

                if (configFact == null)
                    throw new NotFoundException($"No verification facts are found for {entityType}");

                if (configFact.Methods == null || !configFact.Methods.Any())
                    return;

                foreach (var method in configFact.Methods)
                {
                    foreach (var methodSource in method.Sources)
                    {
                        var documents = new List<string>();
                        if (methodSource.Type == SourceType.Form || methodSource.Type == SourceType.Review)
                        {
                            string document;
                            if (methodSource.Settings.TryGetValue("DocumentRequired", out document))
                            {
                                documents.Add(document);
                            }
                            items.Add(new RequiredDocumentResponse { MethodName = method.Name, Documents = documents });
                        }
                    }
                }
            });
            return items;
        }


        public async Task<List<IDocumentResponse>> GetDocuments(string entityType, string entityId)
        {
            var items = new List<IDocumentResponse>();
            var pendingDocuments = await GetPendingDocument(entityType, entityId);
            await Task.Run(() =>
            {

                entityType = entityType.ToLower();
                var configFacts = EnsureFacts(entityType);

                if (configFacts == null)
                    throw new NotFoundException($"No verification facts are found for {entityType}");

                foreach (var item in configFacts)
                {
                    if (item.Methods != null && item.Methods.Any())
                    {

                        foreach (var method in item.Methods)
                        {
                            foreach (var methodSource in method.Sources)
                            {
                                var documents = new List<string>();
                                if (methodSource.Type == SourceType.Form || methodSource.Type == SourceType.Review)
                                {
                                    string document;
                                    bool result = false;
                                    if (methodSource.Settings.TryGetValue("DocumentRequired", out document))
                                    {
                                        var documentPending = pendingDocuments.Where(i => i.DocumentName.Contains(document)).FirstOrDefault();
                                        if (documentPending != null)
                                            result = true;
                                        documents.Add(document);
                                    }
                                    if (document != null)
                                        items.Add(new DocumentResponse { FactName = item.Name, MethodName = method.Name, DocumentName = documents, IsRequested = result });
                                }
                            }
                        }
                    }
                }
            });
            return items;
        }


        public async Task EventRePublish(string entityType, string entityId)
        {
            var FactsInfo = await VerificationFactRepository.Get(entityType, entityId, CurrentStatus.Initiated);
            if (FactsInfo != null && FactsInfo.Count > 0)
            {
                foreach (var item in FactsInfo)
                {
                    await EventHubClient.Publish(new ManualFactVerificationInitiated(entityType, entityId, item.FactName));
                }
            }

            var MethodsInfo = await VerificationMethodRepository.Get(entityType, entityId);
            if (MethodsInfo != null && MethodsInfo.Count > 0)
            {
                foreach (var item in MethodsInfo)
                {
                    if (item.Documents != null && item.Documents.Count > 0)
                    {
                        var PendingDocuments = await GetPendingDocument(entityType, entityId);
                        await EventHubClient.Publish(new VerificationDocumentReceived(entityType, entityId, item.FactName, PendingDocuments.Count, null, null));// need to incorporate
                    }
                }
            }
            var FactsInfoVerification = await VerificationFactRepository.Get(entityType, entityId);
            if (FactsInfoVerification != null && FactsInfoVerification.Count > 0)
            {
                foreach (var item in FactsInfoVerification)
                {
                    await EventHubClient.Publish(new FactVerificationCompleted { Fact = item });
                }
            }

        }

        #endregion

        #region Private Methods

        private async Task<VerificationResult> ExecuteRule(Dictionary<string, object> data, IMethod method, string entityId, string entityType)
        {
            ProductRuleResult ruleExecutionResult = null;
            //if (string.IsNullOrEmpty(method.VerificationRule?.Name) || string.IsNullOrEmpty(method.VerificationRule?.Version))
            //    throw new InvalidOperationException($"VerificationRule not set for {method.Name}");

            var result = await ProductRuleService.RunRule(entityType, entityId, "product1", method.Name);

            if (result == null)
                throw new VerificationFailedException("Unable to execute rule : " + method.Name);
            ruleExecutionResult = result;
            if (ruleExecutionResult.ExceptionDetail != null && ruleExecutionResult.ExceptionDetail.Count > 0)
            {
                throw new VerificationFailedException("Unable to Verify method : " + method.Name);
            }

            if (ruleExecutionResult.Result == Result.Passed)
                return VerificationResult.Passed;
            if (ruleExecutionResult.Result == Result.Failed)
                return VerificationResult.Failed;

            return VerificationResult.UnableToSpecify;
        }

        private bool CanExecuteRule(IMethod method, IVerificationMethod verificationMethod)
        {
            var totalSource = method.Sources.Count;
            var currentCount = verificationMethod.VerificationSources.Select(v => v.SourceName).Distinct().Count();
            return totalSource == currentCount;
        }

        private Dictionary<string, object> ExtractedDataSources(IVerificationMethod verificationMethod)
        {
            var extractedDataSources = new Dictionary<string, object>();

            var sources = verificationMethod.VerificationSources.GroupBy(r => r.SourceName).Select(g => g.OrderByDescending(x => x.DataExtractedDate.Time).First());

            foreach (var source in sources)
            {
                extractedDataSources.Add(source.SourceName, source.DataExtracted);
            }

            return extractedDataSources;
        }

        private async Task ChangeFactStatus(string entityType, string entityId, string factName, string currentUser, CurrentStatus status, string CurrentInitiateMethod)
        {
            var verificationFact = await VerificationFactRepository.Get(entityType, entityId, factName);

            if (verificationFact == null)
            {
                verificationFact = new VerificationFacts()
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    FactName = factName,
                    LastVerifiedBy = currentUser,
                    LastVerifiedDate = new TimeBucket(TenantTime.Now),
                    CurrentStatus = status,
                    CurrentInitiatedMethod = CurrentInitiateMethod
                };

            }
            else
            {
                if (verificationFact.CurrentStatus == CurrentStatus.Initiated || status == CurrentStatus.Initiated || verificationFact.CurrentStatus == CurrentStatus.InProgress)
                {
                    verificationFact.CurrentStatus = status;
                    verificationFact.VerificationStatus = VerificationStatus.None;
                    verificationFact.LastVerifiedDate = new TimeBucket(TenantTime.Now);
                    verificationFact.LastVerifiedBy = currentUser;
                    verificationFact.CurrentInitiatedMethod = CurrentInitiateMethod;
                }
             
            }
            await VerificationFactRepository.AddOrUpdate(verificationFact);
        }

        private static IMethod EnsureFactMethod(IFact fact, string methodName)
        {
            var method = fact.Methods.FirstOrDefault(m => m.Name.Equals(methodName, StringComparison.OrdinalIgnoreCase));
            if (method == null)
                throw new NotFoundException($"{methodName} is not found for {fact.Name} in configuration.");
            return method;
        }

        private Fact EnsureFact(string entityType, string factName)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("EntityType is required.");

            if (string.IsNullOrWhiteSpace(factName))
                throw new ArgumentException("FactName is required.");

            var facts = EnsureFacts(entityType);

            var configFact = facts.FirstOrDefault(e => e.Name.Equals(factName, StringComparison.OrdinalIgnoreCase));

            if (configFact == null)
                throw new NotFoundException($"The {factName} not found for {entityType}");

            return configFact;
        }

        private IEnumerable<Fact> EnsureFacts(string entityType)
        {
            if (Configuration == null)
                throw new NotFoundException("Configuration is not set.");

            if (!Configuration.Facts.ContainsKey(entityType))
                throw new NotFoundException($"The fact not found for {entityType}");

            var facts = Configuration.Facts[entityType];

            if (facts == null || !facts.Any())
                throw new NotFoundException($"The fact not found for {entityType}");
            return facts;
        }

        private string EnsureCurrentUser()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            var username = token?.Subject;
            if (string.IsNullOrWhiteSpace(token?.Subject))
                throw new ArgumentException("User is not authorized");
            return username;
        }

        private static string GetResultValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.result;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }

        private static string GetResultValueForManual(dynamic data)
        {
            Func<dynamic> resultProperty = () => data;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }

        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }



        #endregion
    }
}

