﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationFacts : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string FactName { get; set; }
        CurrentStatus CurrentStatus { get; set; }
        VerificationStatus VerificationStatus { get; set; }
        TimeBucket LastVerifiedDate { get; set; }
        string LastVerifiedBy { get; set; }

        string CurrentInitiatedMethod { get; set; }
    }
}
