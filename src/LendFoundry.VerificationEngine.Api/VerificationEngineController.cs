﻿using LendFoundry.Foundation.Services;
using System.Threading.Tasks;
using RestSharp.Extensions;
using System.Collections.Generic;
using System.Net;
using System;
using System.Linq;
using System.Net.Http.Headers;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
#endif

namespace LendFoundry.VerificationEngine.Api
{
    [Route("/")]
    public class VerificationEngineController : ExtendedController
    {
        public VerificationEngineController(IVerificationEngineService verificationEngineService)
        {
            VerificationEngineService = verificationEngineService;
        }
        private static NoContentResult NoContentResult { get; } = new NoContentResult();
        private IVerificationEngineService VerificationEngineService { get; }

        [HttpPost("{entityType}/{entityId}/{fact}/{method}/{source}")]
        #if DOTNET2
        [ProducesResponseType(typeof(IVerificationMethod), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        #endif
        public async Task<IActionResult> VerifyFacts(string entityType, string entityId, string fact, string method, string source, [FromBody]object request)
        {
            return await ExecuteAsync(async () => Ok((await VerificationEngineService.Verify(entityType, entityId, fact, method, source, request))));
        }

        [HttpGet("{entityType}/{entityId}/{fact}/status")]
        #if DOTNET2
         [Produces("application/json", Type = typeof(IVerificationFacts))]
        [ProducesResponseType(typeof(IVerificationMethod), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]  
        #endif      
        public async Task<IActionResult> GetVerificationStatus(string entityType, string entityId, string fact)
        {
            return await ExecuteAsync(async () => Ok((await VerificationEngineService.GetStatus(entityType, entityId, fact))));
        }

        [HttpGet("{entityType}/{entityId}/{fact}/detail")]
        #if DOTNET2
        [ProducesResponseType(typeof(List<IVerificationMethod>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        #endif
        public async Task<IActionResult> GetVerificationDetail(string entityType, string entityId, string fact)
        {
            return await ExecuteAsync(async () => Ok((await VerificationEngineService.GetDetails(entityType, entityId, fact))));
        }

        [HttpGet("{entityType}/{entityId}")]
        #if DOTNET2
        [ProducesResponseType(typeof(List<IFactResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        #endif
        public async Task<IActionResult> GetVerificationMethod(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok((await VerificationEngineService.GetFactVerification(entityType, entityId))));
        }

        [HttpGet("{entityType}")]
        #if DOTNET2
        [ProducesResponseType(typeof(List<Configuration.IFact>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        #endif
        public async Task<IActionResult> GetVerificationFacts(string entityType)
        {
            return await ExecuteAsync(async () => Ok(await VerificationEngineService.GetFactList(entityType)));
        }

        [HttpPost("{entityType}/{entityId}/{fact}/{methodName}/initiate-manualverification")]
        #if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        #endif
        public async Task<IActionResult> InitiateManualVerification(string entityType, string entityId, string fact,string methodName)
        {
            return await ExecuteAsync(async () =>
            {
                await VerificationEngineService.InitiateManualVerification(entityType, entityId, fact, methodName);
                return new NoContentResult();
            });
        }

        [HttpPost("{entityType}/{entityId}/{fact}/{method}/upload-document")]
        #if DOTNET2
        [ProducesResponseType(typeof(Application.Document.IApplicationDocument), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        #endif
        public async Task<IActionResult> VerificationDocumentReceived(string entityType, string entityId, string fact, string method, string source, string category, IFormFile file)
        {
            if (file == null)
                return ErrorResult.BadRequest("Please upload file.");
            var fileName = ContentDispositionHeaderValue
                       .Parse(file.ContentDisposition)
                       .FileName
                       .Trim('"');

            return await ExecuteAsync(async () =>
                Ok(await VerificationEngineService.AddDocument(entityType, entityId, fact, method,
                    file.OpenReadStream().ReadAsBytes(), fileName)));
        }

        [HttpGet("{entityType}/{entityId}/pending-document")]
        #if DOTNET2
         [ProducesResponseType(typeof(List<IPendingDocumentResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        #endif
        public async Task<IActionResult> PendingDocument(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
                    Ok(await VerificationEngineService.GetPendingDocument(entityType, entityId)));
        }

        [HttpGet("{entityType}/{fact}/required-document")]
        #if DOTNET2
        [ProducesResponseType(typeof(List<IPendingDocumentResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        #endif
        public async Task<IActionResult> GetRequiredDocumet(string entityType, string fact)
        {
            return await ExecuteAsync(async () =>
                    Ok(await VerificationEngineService.GetRequiredDocumet(entityType, fact)));
        }

        [HttpGet("{entityType}/{entityId}/documents")]
        #if DOTNET2
        [ProducesResponseType(typeof(List<IDocumentResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        #endif
        public async Task<IActionResult> GetDocuments(string entityType,string entityId)
        {
            return await ExecuteAsync(async () =>
                    Ok(await VerificationEngineService.GetDocuments(entityType, entityId)));
        }

        [HttpGet("{entityType}/{entityId}/{factName}/documentsDetails")]
        #if DOTNET2
        [ProducesResponseType(typeof(List<Application.Document.IDocumentWithApplication>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        #endif
        public async Task<IActionResult> GetDocumentsByFact(string entityType, string entityId,string factName)
        {
            return await ExecuteAsync(async () =>
                    Ok(await VerificationEngineService.GetDocumentsByFact(entityType, entityId, factName)));
        }

        [HttpPut("{entityType}/{entityId}/{factName}/{documentId}/{status}/{*reasons}")]
        #if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        #endif
        public async Task<IActionResult> VerifyDocument(string entityType, string entityId, string factName,string documentId,string status,string reasons) =>
           await  ExecuteAsync(async () => { await VerificationEngineService.VerifyDocument(entityType, entityId, factName, documentId, status, SplitReasons(reasons)); return NoContentResult; });
            
        

        [HttpPost("{entityType}/{entityId}/events-republish")]
        #if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        #endif
        public async Task<IActionResult> EventRePublish(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                await VerificationEngineService.EventRePublish(entityType, entityId);
                return new NoContentResult();
            });
        }

        private static List<string> SplitReasons(string reasons)
        {
            if (string.IsNullOrWhiteSpace(reasons))
                return null;

            reasons = WebUtility.UrlDecode(reasons);

            return string.IsNullOrWhiteSpace(reasons)
                ? null
                : reasons.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        //[HttpGet("{entityType}/{entityId}")]
        //public async Task<IActionResult> GetVerificationFacts(string entityType, string entityId)
        //{
        //    return await ExecuteAsync(async () => Ok(await VerificationEngineService.Get(entityType)));
        //}

        /*
            
        */
    }
}
