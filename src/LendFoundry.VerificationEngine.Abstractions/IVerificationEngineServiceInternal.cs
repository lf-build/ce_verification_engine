﻿using System.Threading.Tasks;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationEngineServiceInternal : IVerificationEngineService
    {
        Task<IVerificationMethod> Verify(string entityType, string entityId, string factName, string methodName, string sourceName, bool isManual, object request);
    }
}