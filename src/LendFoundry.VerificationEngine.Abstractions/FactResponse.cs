﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using LendFoundry.Foundation.Date;

namespace LendFoundry.VerificationEngine
{
    public class FactResponse : IFactResponse
    {
        public FactResponse()
        {
            Attempts = new List<IMethodAttemptResult>();
            VerificationSources = new List<IVerificationSources>();
        }

        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string FactName { get; set; }
        public CurrentStatus CurrentStatus { get; set; }
        public VerificationStatus VerificationStatus { get; set; }

        public string CurrentInitiatedMethod { get; set; }
        public TimeBucket LastVerifiedDate { get; set; }
        public string LastVerifiedBy { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IVerificationSources, VerificationSources>))]
        public List<IVerificationSources> VerificationSources { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IMethodAttemptResult, MethodAttemptResult>))]
        public List<IMethodAttemptResult> Attempts { get; set; }
    }
}
