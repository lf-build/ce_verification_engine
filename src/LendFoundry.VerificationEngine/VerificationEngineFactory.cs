﻿using System;
using LendFoundry.VerificationEngine.Configuration;
using LendFoundry.Configuration.Client;

using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.EventHub;
#else
using Microsoft.Framework.DependencyInjection;
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.DataAttributes.Client;
using LendFoundry.ProductRule.Client;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Application.Document.Client;
using LendFoundry.Configuration;

namespace LendFoundry.VerificationEngine
{
    public class VerificationEngineServiceFactory : IVerificationEngineServiceFactory
    {
        public VerificationEngineServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }
        public IVerificationEngineServiceInternal Create(ITokenReader reader, ITokenHandler handler, ILogger logger, VerificationConfiguration verificationConfiguration)
        {
            
            var eventHubServiceFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHubService = eventHubServiceFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTimeService = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var productRuleFactory = Provider.GetService<IProductRuleServiceClientFactory>();
            var productRuleService = productRuleFactory.Create(reader);          

            var lookUpFactory = Provider.GetService<ILookupClientFactory>();
            var lookUpService = lookUpFactory.Create(reader);

            var applicationDocument = Provider.GetService<IApplicationDocumentServiceClientFactory>();
            var applicationDocumentService = applicationDocument.Create(reader);

            var factRepositoryFactory = Provider.GetService<IVerificationFactsRepositoryFactory>();
            var factRepository = factRepositoryFactory.Create(reader);

            var methodRepositoryFactory = Provider.GetService<IVerificationMethodRepositoryFactory>();
            var methodRepository = methodRepositoryFactory.Create(reader);

            var datatAttribute = Provider.GetService<IDataAttributesClientFactory>();
            var datatAttributeService = datatAttribute.Create(reader);

            var identityServiceFactory = Provider.GetService<IIdentityServiceFactory>();
            var identityEngineService = identityServiceFactory.Create(reader);

       

            return new VerificationEngineService(factRepository, eventHubService, tenantTimeService, applicationDocumentService, verificationConfiguration, productRuleService,lookUpService, identityEngineService, reader, handler, methodRepository, datatAttributeService);
        }
    }
}
