﻿namespace LendFoundry.VerificationEngine
{
    public enum VerificationResult
    {
        None = 0,
        Failed = 1,
        Passed = 2,
        UnableToSpecify = 3
    }
}
