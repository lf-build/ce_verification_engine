﻿namespace LendFoundry.VerificationEngine
{
    public interface IFactVerificationRequest
    {
        VerificationResult Result { get; set; }
        object Data { get; set; }
    }
}
