﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.VerificationEngine
{
    public class RequiredDocumentResponse : IRequiredDocumentResponse
    {
        public string MethodName { get; set; }
        public List<string> Documents { get; set; }
    }
}
