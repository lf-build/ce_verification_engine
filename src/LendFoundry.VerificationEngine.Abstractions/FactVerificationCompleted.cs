﻿namespace LendFoundry.VerificationEngine
{
    public class FactVerificationCompleted
    {
        public object Fact { get; set; }
    }
}
