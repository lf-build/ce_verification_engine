﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.VerificationEngine
{
    public static class Settings
    {
        public static string ServiceName { get; } = "verification-engine";

        private static string Prefix { get; } = ServiceName.ToUpper();
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        public static ServiceSettings ApplicationDocument { get; } = new ServiceSettings($"{Prefix}_APPLICATIONDOCUMENT", "application-document");        
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "verification-engine");
        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUPSERVICE", "lookup-service");
        public static ServiceSettings DataAttribute { get; } = new ServiceSettings($"{Prefix}_DATAATTRIBUTES_HOST", "data-attributes", $"{Prefix}_DATAATTRIBUTES_PORT");
        public static ServiceSettings Identity { get; } = new ServiceSettings($"{Prefix}_IDENTITY", "identity");
        public static ServiceSettings ProductService { get; } = new ServiceSettings($"{Prefix}_PRODUCTRULE_HOST", "productrule", $"{Prefix}_PRODUCTRULE_PORT");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";

        public static ServiceSettings StatusManagement { get; } = new ServiceSettings($"{Prefix}_STATUS_MANAGEMENT", "status-management");
    }
}