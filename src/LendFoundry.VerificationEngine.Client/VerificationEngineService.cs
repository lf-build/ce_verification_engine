﻿using LendFoundry.Foundation.Client;

using RestSharp;
using System.Threading.Tasks;
using System.Collections.Generic;
using LendFoundry.VerificationEngine.Configuration;
using System;
using LendFoundry.Application.Document;
using Newtonsoft.Json;

namespace LendFoundry.VerificationEngine.Client
{
    public class VerificationEngineService : IVerificationEngineService
    {
        #region Constructors
        public VerificationEngineService(IServiceClient client)
        {
            Client = client;
        }
        #endregion

        #region Private Properties
        private IServiceClient Client { get; }
        #endregion


        public async Task<IVerificationMethod> Verify(string entityType, string entityId, string factName, string methodName, string sourceName, object verificationRequest)
        {
            var request = new RestRequest("{entityType}/{entityId}/{fact}/{method}/{source}", RestSharp.Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("fact", factName);
            request.AddUrlSegment("method", methodName);
            request.AddUrlSegment("source", sourceName);
            var json = JsonConvert.SerializeObject(verificationRequest);
            request.AddParameter("text/json", json, ParameterType.RequestBody);          
            return await Client.ExecuteAsync<VerificationMethod>(request);
        }

        public async Task<IVerificationFacts> GetStatus(string entityType, string entityId, string factName)
        {
            var request = new RestRequest("{entityType}/{entityId}/{fact}/status", RestSharp.Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("fact", factName);
            return await Client.ExecuteAsync<VerificationFacts>(request);
        }

        public async Task<List<IVerificationMethod>> GetDetails(string entityType, string entityId, string factName)
        {
            var request = new RestRequest("{entityType}/{entityId}/{fact}/detail", RestSharp.Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("fact", factName);
            var result =  await Client.ExecuteAsync<List<VerificationMethod>> (request);
            return new List<IVerificationMethod>(result);
        }

        public async Task<List<IFact>> GetFactList(string entityType)
        {
            var request = new RestRequest("{entityType}", RestSharp.Method.GET);
            request.AddUrlSegment("entityType", entityType);
            var result = await Client.ExecuteAsync<List<Fact>>(request);
            return new List<IFact>(result);
        }

        public async Task<List<IFactResponse>> GetFactVerification(string entityType, string entityId)
        {
            var request = new RestRequest("{entityType}/{entityId}", RestSharp.Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            var result = await Client.ExecuteAsync<List<FactResponse>>(request);
            return new List<IFactResponse>(result);
        }

        public async Task InitiateManualVerification(string entityType, string entityId, string factName,string methodName)
        {
            var request = new RestRequest("{entityType}/{entityId}/{fact}/{methodName}/initiate-manualverification", RestSharp.Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("fact", factName);
            request.AddUrlSegment("methodName", methodName);
            await Client.ExecuteAsync(request);
        }

        public async Task<IApplicationDocument> AddDocument(string entityType, string entityId, string factName, string methodName, byte[] file, string fileName)
        {
            var request = new RestRequest("{entityType}/{entityId}/{fact}/{method}/upload-document", RestSharp.Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("fact", factName);
            request.AddUrlSegment("method", methodName);
            request.AddFile("file", file, fileName, "application/octet-stream");
            return await Client.ExecuteAsync<ApplicationDocument>(request);
        }

        public async Task<List<IRequiredDocumentResponse>> GetRequiredDocumet(string entityType, string factName)
        {
            var request = new RestRequest("{entityType}/{fact}/required-document", RestSharp.Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("fact", factName);
            var result = await Client.ExecuteAsync<List<RequiredDocumentResponse>>(request);
            return new List<IRequiredDocumentResponse>(result);
        }

        public async Task<List<IPendingDocumentResponse>> GetPendingDocument(string entityType, string entityId)
        {
            var request = new RestRequest("{entityType}/{entityId}/pending-document", RestSharp.Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            var result = await Client.ExecuteAsync<List<PendingDocumentResponse>>(request);
            return new List<IPendingDocumentResponse>(result);
        }

        public async Task EventRePublish(string entityType, string entityId)
        {
            var request = new RestRequest("{entityType}/{entityId}/events-republish", RestSharp.Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);          
            await Client.ExecuteAsync(request);
        }

        public async Task<List<IDocumentResponse>> GetDocuments(string entityType, string entityId)
        {
            var request = new RestRequest("{entityType}/{entityId}/documents", RestSharp.Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            var result = await Client.ExecuteAsync<List<IDocumentResponse>>(request);
            return new List<IDocumentResponse>(result);
        }

        public Task<List<IDocumentWithApplication>> GetDocumentsByFact(string entityType, string entityId, string factName)
        {
            throw new NotImplementedException();
        }

        public Task VerifyDocument(string entityType, string entityId, string factName, string documentId, string statusCode, List<string> reasons)
        {
            throw new NotImplementedException();
        }
    }
}
